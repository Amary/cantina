<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class checkTokenUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        if ($request->user()  === null || $request->user()->tokens() === null) {
            return response('unknown', 419);
        }

        return $next($request);
    }
}
