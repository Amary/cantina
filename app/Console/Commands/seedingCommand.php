<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class  seedingCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'seeding';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        \App\Models\User::factory(10)->create();
        \App\Models\Secret::factory(10)->create();

        return 0;
    }
}
