import Login from './components/Login';
import User from './components/User';

export default {
    mode: 'history',
    routes: [
        {
            path: '/',
            component: Login
        },
        {
            path: '/me',
            component: User
        },
    ]
}
