<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_as_user_i_can_use_api()
    {
       Sanctum::actingAs(
            User::factory()->create(),
            ['*']
        );

        $response = $this->get('/api/me');

        $response->assertOk();
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_cannot_use_api_if_i_not_token()
    {
        $response = $this->get('/api/me');

        $response->assertStatus(419);
    }
}
