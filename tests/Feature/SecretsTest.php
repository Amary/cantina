<?php

namespace Tests\Feature;

use App\Models\Secret;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class SecretsTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_as_user_i_can_acces_my_secrets()
    {
        // Create an user authenticated for use the api
        Sanctum::actingAs(
            User::factory()->create(),
            ['*']
        );

        // Create secrets
        Secret::factory(5)->create();

        // Check if 5 secrets is retrieved
        $this->get('/api/secrets')->assertJsonCount(5);
    }
}
