<?php

use App\Http\Controllers\Auth\LogoutController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('app');
});

Route::middleware('auth')->get('/me', function () {
    return view('app');
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/tokens/create', function (Request $request) {
    if($request->user() !== null ) {
        $token = $request->user()->createToken('auth-token');
        return ['token' => $token->plainTextToken];
    }
    return redirect('/');
});
Auth::routes();
