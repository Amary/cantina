<?php

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['auth:sanctum']], function(){

    /**
     * Return the current user
     * @authenticated
     */
    Route::get('/me', function (Request $request) {
        return $request->user();
    })->name('api.me');

    /**
     * Return the secrets of the user
     * @authenticated
     */
    Route::get('/secrets', function(Request $request){
        return $request->user()->secrets;
    })->name('api.secrets');

    /**
     * Return the list of users
     * @authenticated
     */
    Route::get('/users', function(){
        return User::all();
    })->name('api.users');

});
